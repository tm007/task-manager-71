package ru.tsc.apozdnov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.exception.system.EmptyStatusException;
import ru.tsc.apozdnov.tm.exception.system.IncorrectStatusException;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static String toName(final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    @NotNull
    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty()) throw new EmptyStatusException();
        for (final Status status : values()) {
            if (status.name().equals(value)) return status;
        }
        throw new IncorrectStatusException(value);
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }
}