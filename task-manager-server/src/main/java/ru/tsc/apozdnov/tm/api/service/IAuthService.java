package ru.tsc.apozdnov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.model.SessionDtoModel;
import ru.tsc.apozdnov.tm.dto.model.UserDtoModel;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    SessionDtoModel validateToken(@Nullable String token);

    @NotNull
    UserDtoModel registry(@Nullable String login, @Nullable String password, @Nullable String email);

}