package ru.tsc.apozdnov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
